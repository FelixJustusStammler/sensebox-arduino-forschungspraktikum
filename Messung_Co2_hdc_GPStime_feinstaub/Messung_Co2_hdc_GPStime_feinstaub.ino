#include <RV8523.h>
#include <senseBoxIO.h>
//#include "SenseBoxMCU.h"
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h> // http://librarymanager/All#Adafruit_GFX_Library
#include <Adafruit_SSD1306.h> // http://librarymanager/All#Adafruit_SSD1306
#include <SD.h>
#include <SparkFun_u-blox_GNSS_Arduino_Library.h> // http://librarymanager/All#SparkFun_u-blox_GNSS_Arduino_Library
#include <SparkFun_SCD30_Arduino_Library.h> // http://librarymanager/All#SparkFun_SCD30_Arduino_Library
#include <Adafruit_HDC1000.h> // http://librarymanager/All#Adafruit_HDC1000_Library
#include <string.h>
#include <Time.h>
#include <TinyGPS.h>
#include <SdsDustSensor.h> 
#include <Adafruit_NeoPixel.h>
 Adafruit_NeoPixel rgb_led_1= Adafruit_NeoPixel(3, 1,NEO_GRB + NEO_KHZ800);


char timestamp[20];
char tsBuffer[20];
char tsBuffer_short[21];

SdsDustSensor sds(Serial1);
RV8523 rtc;

#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 64
#define OLED_RESET -1
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);
SFE_UBLOX_GNSS myGNSS;
 
 //GPS Timestamp
char* getTimeStamp()
{
    sprintf(tsBuffer, "%04d-%02d-%02d %02d:%02d:%02d",
     myGNSS.getYear(), myGNSS.getMonth(), myGNSS.getDay(), myGNSS.getHour()+2, myGNSS.getMinute(), myGNSS.getSecond());
return tsBuffer;
}

//GPS Timestamp short
char* getTimeStampGPS_short()
{
  if (myGNSS.getTimeValid() == true)
    {
    sprintf(tsBuffer, "%02d,%02d,%02d,",
    myGNSS.getHour()+2, myGNSS.getMinute(), myGNSS.getSecond());
    }
return tsBuffer;
}


File ges;
File temp_ex;
File humi_ex;
File Co2;
File temp;
File humi;
SCD30 airSensor;
Adafruit_HDC1000 hdc = Adafruit_HDC1000();
//TinyGPS gps;


float lat; //Geografische Breite
float lng; //Geografische Länge
float alt; //Höhe über Meeresspiegel in Metern
float speed;
float date;


void setup() {

senseBoxIO.powerI2C(true);
delay(2000);
display.begin(SSD1306_SWITCHCAPVCC, 0x3D);
display.display();
delay(100);
display.clearDisplay();
SD.begin(28);
rtc.begin();


//GPS
  Wire.begin();

  if (myGNSS.begin() == false) //Connect to the Ublox module using Wire port
  {
    Serial.println(F("Ublox GPS not detected at default I2C address. Please check wiring. Freezing."));
    while (1);
  }


//Zeitstempel der den Tag an gibt und bei jeder neuen Messung einmal aufgeschrieben wird
ges = SD.open("ges.txt", FILE_WRITE);
    ges.print(myGNSS.getYear());
    ges.print("-");
     if (myGNSS.getMonth()<10){
    ges.print("0");
    }
    ges.print(myGNSS.getMonth());
    ges.print("-");
    if (myGNSS.getDay()<10){
    ges.print("0");
    }
    ges.print(myGNSS.getDay());
    ges.println('PM');
    ges.println("hour, min, sec, ex_temp, ex_humi, CO2, temp, humi, PM2.5, PM10, latitude, longitude");
    ges.println("");
  ges.close();

 Wire.begin();
  if (airSensor.begin() == false)
  {
    while (1)
      ;
  }
hdc.begin();

  myGNSS.setI2COutput(COM_TYPE_UBX); //Set the I2C port to output UBX only (turn off NMEA noise)
  myGNSS.saveConfiguration(); //Save the current settings to flash and BBR

  airSensor.setTemperatureOffset(2.5);
  //airSensor.setTemperatureOffset(3);
 //  airSensor.setAmbientPressure(1001);
 //airSensor.setAltitudeCompensation(125); 
  
int co2;


pinMode(7, OUTPUT);
  rgb_led_1.begin();
  rgb_led_1.setBrightness(999);

  digitalWrite(7,HIGH);

sds.begin();
sds.setQueryReportingMode();


}

void loop() {
//Temperaturoffset abhängig vom gemessenen ppm zahl anpasssen

PmResult pm = sds.queryPm();
delay(5000);
// Zeitstempel die vor dem Messwert aufgetragen werden.
//Zeit sind nur Stunden, Minuten und Sekunden.
//Alle Messwerte werden in die Datei ges geschrieben
// alle 12 sek wird gemessen
int co2= airSensor.getCO2();
float temp =airSensor.getTemperature();
float humi = airSensor.getHumidity();
ges = SD.open("ges.txt", FILE_WRITE);
 ges.print(String(getTimeStampGPS_short()) +"   "+ String(hdc.readTemperature())+" , "+ String(hdc.readHumidity())+" , ");
 ges.print(String(co2)+" , "+String(temp)+" , "+String(humi)+" , "+String(pm.pm25)+" , "+String(pm.pm10));
 ges.println(" , "+String(myGNSS.getLatitude())+" , "+String(myGNSS.getLongitude()));
ges.close();

delay(5000);



//Display Anzeige
  display.setCursor(0,0);
  display.setTextSize(1);
  display.setTextColor(WHITE,BLACK);
  display.println(getTimeStamp());

  display.setCursor(0,16);
  display.setTextSize(1);
  display.setTextColor(WHITE,BLACK);
  display.println(co2);
  display.setCursor(0,32);
  display.setTextSize(1);
  display.setTextColor(WHITE,BLACK);
  display.println(temp);
  display.setCursor(50,32);
  display.setTextSize(1);
  display.setTextColor(WHITE,BLACK);
  display.println(hdc.readTemperature());
    display.setCursor(0,48);
  display.setTextSize(1);
  display.setTextColor(WHITE,BLACK);
  display.println(humi);
  display.setCursor(50,48);
  display.setTextSize(1);
  display.setTextColor(WHITE,BLACK);
  display.println(hdc.readHumidity());
display.display();

      display.setCursor(32,16);
       display.setTextSize(1);
  display.setTextColor(WHITE,BLACK);
        display.println(pm.pm25);
        display.display();

       display.setCursor(0,55);
       display.setTextSize(1);
  display.setTextColor(WHITE,BLACK);
        display.println(pm.pm10);
        display.display();




display.clearDisplay();

if (co2 < 600)
  {
    rgb_led_1.setPixelColor(0,rgb_led_1.Color(0,255,0));
    rgb_led_1.show();
  } else if (co2 < 800)
  {
    rgb_led_1.setPixelColor(0,rgb_led_1.Color(255,120,11));
    rgb_led_1.show();
  } else 
  {
    rgb_led_1.setPixelColor(0,rgb_led_1.Color(255,0,0));
    rgb_led_1.show();
  }

}
