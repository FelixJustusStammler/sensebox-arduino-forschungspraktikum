/*
 * Arduino ins Freie bringen.
 * Nach 4 Minuten erfolgt die Kalibrierung auf 450 ppm
 * Er benötigt konstante Bedingungen und sollte schon etwas warmgelaufen sein. 
 * Wenn man ihn von einem warmen Raum ins kalte nach draußen bringt, sind die vier Minuten u. U. nicht ausreichend.
*/


#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <senseBoxIO.h>
//#include "SenseBoxMCU.h"
#include "SparkFun_SCD30_Arduino_Library.h"

boolean unkalibriert;
long laufzeit;

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);
SCD30 airSensor;

void setup() {
  senseBoxIO.powerI2C(true);
  delay(2000);
  display.begin(SSD1306_SWITCHCAPVCC, 0x3D);
  display.display();
  delay(100);
  display.clearDisplay();
   Wire.begin();
   Wire.setClock(50000); //50kHz

  if (airSensor.begin() == false)
  {
    while (1)
      ;
  }

  unkalibriert = true;
  display.setCursor(0,0);
  display.setTextSize(1);
  display.setTextColor(WHITE,BLACK);
  display.println("Zur Kalibrierung den Sensor nach draussen bringen");
  display.display();
  delay(60000);

}

void loop() {

if (unkalibriert) {
    display.clearDisplay();
      display.setCursor(0,0);
      display.setTextSize(1);
      display.setTextColor(WHITE,BLACK);
      display.println("Kalibrierung laeuft");
    display.display();
  } else {
    display.clearDisplay();
      display.setCursor(0,0);
      display.setTextSize(1);
      display.setTextColor(WHITE,BLACK);
      display.println("Kalibrierung abgeschlossen");
    display.display();
  }
  delay(1000);
  display.clearDisplay();
    display.setCursor(0,0);
    display.setTextSize(2);
    display.setTextColor(WHITE,BLACK);
    display.println(airSensor.getCO2());
  display.display();
  delay(1000);
  laufzeit = millis();
  if (unkalibriert && laufzeit > 240000) {    //nach 4 min erfolgt Kalibrierung
    airSensor.setForcedRecalibrationFactor(450);
    unkalibriert = false;
  }

}
