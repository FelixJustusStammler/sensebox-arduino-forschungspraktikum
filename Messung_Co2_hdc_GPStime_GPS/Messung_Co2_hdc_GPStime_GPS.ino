#include <RV8523.h>
#include <senseBoxIO.h>
//#include "SenseBoxMCU.h"
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h> // http://librarymanager/All#Adafruit_GFX_Library
#include <Adafruit_SSD1306.h> // http://librarymanager/All#Adafruit_SSD1306
#include <SD.h>
#include <SparkFun_u-blox_GNSS_Arduino_Library.h> // http://librarymanager/All#SparkFun_u-blox_GNSS_Arduino_Library
#include <SparkFun_SCD30_Arduino_Library.h> // http://librarymanager/All#SparkFun_SCD30_Arduino_Library
#include <Adafruit_HDC1000.h> // http://librarymanager/All#Adafruit_HDC1000_Library
#include <string.h>
#include <Time.h>
#include <TinyGPS.h>


char timestamp[20];
char tsBuffer[20];
char tsBuffer_short[21];

RV8523 rtc;
#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 64
#define OLED_RESET -1
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);
SFE_UBLOX_GNSS myGNSS;
 
 //GPS Timestamp
char* getTimeStamp()
{
    sprintf(tsBuffer, "%04d-%02d-%02d %02d:%02d:%02d",
     myGNSS.getYear(), myGNSS.getMonth(), myGNSS.getDay(), myGNSS.getHour()+2, myGNSS.getMinute(), myGNSS.getSecond());
return tsBuffer;
}

//GPS Timestamp short
char* getTimeStampGPS_short()
{
  if (myGNSS.getTimeValid() == true)
    {
    sprintf(tsBuffer, "%02d,%02d,%02d,",
    myGNSS.getHour()+2, myGNSS.getMinute(), myGNSS.getSecond());
    }
return tsBuffer;
}


File ges;
File temp_ex;
File humi_ex;
File Co2;
File temp;
File humi;
SCD30 airSensor;
Adafruit_HDC1000 hdc = Adafruit_HDC1000();
//TinyGPS gps;


float lat; //Geografische Breite
float lng; //Geografische Länge
float alt; //Höhe über Meeresspiegel in Metern
float speed;
float date;



void setup() {

senseBoxIO.powerI2C(true);
delay(2000);
display.begin(SSD1306_SWITCHCAPVCC, 0x3D);
display.display();
delay(100);
display.clearDisplay();
SD.begin(28);
rtc.begin();




//Zeitstempel der den Tag an gibt und bei jeder neuen Messung einmal aufgeschrieben wird
ges = SD.open("ges.txt", FILE_WRITE);
//    ges.print(myGNSS.getYear());
//    ges.print(myGNSS.getMonth());
//    ges.println(myGNSS.getDay());
    ges.println("hour, min, sec, ex_temp, ex_humi, CO2, temp, humi, latitude, longitude");
    ges.println("");
  ges.close();
  
 Wire.begin();
  if (airSensor.begin() == false)
  {
    while (1)
      ;
  }
hdc.begin();

//GPS
  Wire.begin();

  if (myGNSS.begin() == false) //Connect to the Ublox module using Wire port
  {
    Serial.println(F("Ublox GPS not detected at default I2C address. Please check wiring. Freezing."));
    while (1);
  }

  myGNSS.setI2COutput(COM_TYPE_UBX); //Set the I2C port to output UBX only (turn off NMEA noise)
  myGNSS.saveConfiguration(); //Save the current settings to flash and BBR

  airSensor.setTemperatureOffset(2.5);
  //airSensor.setTemperatureOffset(3);
 //  airSensor.setAmbientPressure(1001);
 //airSensor.setAltitudeCompensation(125); 
  
int co2;

}

void loop() {
//Temperaturoffset abhängig vom gemessenen ppm zahl anpasssen

delay(5000);
// Zeitstempel die vor dem Messwert aufgetragen werden.
//Zeit sind nur Stunden, Minuten und Sekunden.
//Alle Messwerte werden in die Datei ges geschrieben
// alle 12 sek wird gemessen

ges = SD.open("ges.txt", FILE_WRITE);
 ges.print(String(getTimeStampGPS_short()) +"   "+ String(hdc.readTemperature())+" , "+ String(hdc.readHumidity())+" , "+ String(airSensor.getCO2())+" , "+String(airSensor.getTemperature())+" , "+String(airSensor.getHumidity()));
 ges.println(" , "+String(myGNSS.getLatitude())+" , "+String(myGNSS.getLongitude()));
ges.close();

delay(5000);
//Display Anzeige
  display.setCursor(0,0);
  display.setTextSize(1);
  display.setTextColor(WHITE,BLACK);
  display.println(getTimeStamp());

  display.setCursor(0,16);
  display.setTextSize(1);
  display.setTextColor(WHITE,BLACK);
  display.println(airSensor.getCO2());
  display.setCursor(0,32);
  display.setTextSize(1);
  display.setTextColor(WHITE,BLACK);
  display.println(airSensor.getTemperature());
  display.setCursor(50,32);
  display.setTextSize(1);
  display.setTextColor(WHITE,BLACK);
  display.println(hdc.readTemperature());
    display.setCursor(0,48);
  display.setTextSize(1);
  display.setTextColor(WHITE,BLACK);
  display.println(airSensor.getHumidity());
  display.setCursor(50,48);
  display.setTextSize(1);
  display.setTextColor(WHITE,BLACK);
  display.println(hdc.readHumidity());
display.display();

      display.setCursor(32,16);
       display.setTextSize(1);
  display.setTextColor(WHITE,BLACK);
        display.println(airSensor.getTemperatureOffset());
        display.display();



display.clearDisplay();

}
